#ifndef GAME_H
#define GAME_H

#include <future>
#include "SpriteBatch.h"
#include "SpriteFont.h"

#include "Mesh.h"
#include "Model.h"
#include "singleton.h"

class Game : public Singleton<Game>
{
public:
	Game() {}
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	const DirectX::SimpleMath::Vector3 mDefCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);
	DirectX::SimpleMath::Vector3 mCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);
	std::vector<Model> mModels;
	enum Modelid { FLOOR, BACK_WALL, LEFT_WALL, BOX, CROSS, CROSS2, WINDOW, WINDOW2, ROCK, DRAGON, SUCK, SCIENTIST, TOTAL=12 }; 

private:

	//data to manage 2nd thread loading
	struct LoadData
	{
		std::future<void> loader;
		int totalToLoad = 0;
		int loadedSoFar = 0;
		bool running = false;
	};
	LoadData mLoadData;
	float gAngle = 0;
	DirectX::SpriteBatch *pFontBatch = nullptr;
	DirectX::SpriteFont *pFont = nullptr;

	void Load();
	void RenderLoad(float dTime);
};

#endif
